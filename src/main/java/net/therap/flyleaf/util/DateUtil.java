package net.therap.flyleaf.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author shafin
 * @since 12/27/16
 */
public class DateUtil {

    public static String formatDate(Date date, String format) {
        if (date != null) {
            DateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(date);
        }

        return "";
    }
}
