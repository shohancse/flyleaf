package net.therap.flyleaf.util;

import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.enumerator.Gender;
import net.therap.flyleaf.service.RoleService;
import net.therap.flyleaf.service.UserService;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Date;

/**
 * @author shafin
 * @since 12/27/16
 */
public class DataInitialization {

    //@Autowired
    private UserService userService;

    //@Autowired
    private RoleService roleService;

    public void onApplicationEvent(ContextRefreshedEvent event) {
        String adminEmail = "flyleaf.store@gmail.com";
        String adminPassword = "therap";

        User user = userService.getUser(adminEmail);
        if (user == null) {

            user.setFirstName("Flyleaf");
            user.setLastName("Admin");
            user.setAddress("Dhaka");
            user.setGender(Gender.MALE);
            user.setDateOfBirth(new Date());
            user.setPhone("01710000000");
            user.setApproved(true);
            user.setCreatedAt(new Date());
            user.setPassword(EncryptionUtil.generateSecureHash(adminPassword));

            user.setRoleList(roleService.getAdminRoleList());

            userService.insertUser(user);
        }
    }
}
