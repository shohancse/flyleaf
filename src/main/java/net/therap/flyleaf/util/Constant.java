package net.therap.flyleaf.util;

/**
 * @author arafat
 * @since 12/25/16
 */
public class Constant {

    public static final double THRESHOLD_RATING = 3.5;
    public static final String AVERAGE_RATING = "avg_rating";
}
