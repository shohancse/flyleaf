package net.therap.flyleaf.web.helper;

import net.therap.flyleaf.domain.AuthToken;
import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.service.AuthorizationService;
import net.therap.flyleaf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;

/**
 * @author shafin
 * @since 12/26/16
 */
@Component
public class CookieManager {

    public static final String AUTH_COOKIE_NAME = "AUTHENTICATION_VALUE";
    public static final int COOKIE_AGE = 24 * 60 * 60 * 1000;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authService;

    public User getUserByValidatingCookie(Cookie[] cookies) {

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {

                User user = authService.getUserByAuthCookie(cookie.getValue());
                if (user != null) {

                    return user;
                }
            }
        }

        return null;
    }

    public Cookie setAuthCookie(String userEmail) {

        AuthToken token = authService.setAuthToken(userEmail);
        Cookie cookie = new Cookie(AUTH_COOKIE_NAME, token.getToken());
        cookie.setMaxAge((int) token.getExpiresOn().getTime());
        return cookie;
    }

    public Cookie invalidateAuthCookie(long userId, Cookie[] cookies) {

        authService.removeAllAuthTokens(userId);

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {

                cookie.setMaxAge(0);
                cookie.setValue(null);
                return cookie;
            }
        }

        return null;
    }
}
