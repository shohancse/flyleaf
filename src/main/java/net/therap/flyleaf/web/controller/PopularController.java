package net.therap.flyleaf.web.controller;

import net.therap.flyleaf.service.AuthorService;
import net.therap.flyleaf.service.category.UserCategoryService;
import net.therap.flyleaf.service.product.CustomerProductService;
import net.therap.flyleaf.service.publisher.PublisherService;
import net.therap.flyleaf.service.storekeeper.MyStoreService;
import net.therap.flyleaf.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author arafat
 * @since 12/22/16
 */
@Controller
public class PopularController {

    @Autowired
    private UserCategoryService userCategoryService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private PublisherService publisherService;

    @Autowired
    private MyStoreService storeService;

    @Autowired
    private CustomerProductService productService;

    @RequestMapping(value = "/popular/categories", method = RequestMethod.GET)
    public String getCategories(Model model) {

        model.addAttribute("popularObjectList", userCategoryService.getPopularCategories());
        model.addAttribute("type", "categories");
        addTopRatedBooksToModel(model);

        return "home";
    }

    @RequestMapping(value = "/popular/authors", method = RequestMethod.GET)
    public String getAuthors(Model model) {

        model.addAttribute("popularObjectList", authorService.getPopularAuthors());
        model.addAttribute("type", "authors");
        addTopRatedBooksToModel(model);

        return "home";
    }

    @RequestMapping(value = "/popular/publishers", method = RequestMethod.GET)
    public String getPublishers(Model model) {

        model.addAttribute("popularObjectList", publisherService.getPopularPublishers());
        model.addAttribute("type", "publishers");
        addTopRatedBooksToModel(model);

        return "home";
    }

    @RequestMapping(value = "/popular/stores", method = RequestMethod.GET)
    public String getStores(Model model) {

        model.addAttribute("popularObjectList", storeService.getPopularStores());
        model.addAttribute("type", "stores");
        addTopRatedBooksToModel(model);

        return "home";
    }

    private void addTopRatedBooksToModel(Model model) {

        model.addAttribute("topRatedBookList", productService.getProductsByTopRating(Constant.AVERAGE_RATING,
                Constant.THRESHOLD_RATING));
        model.addAttribute("topRatedProductListForCarousal", productService.getProductsByTopRating(Constant.AVERAGE_RATING,
                Constant.THRESHOLD_RATING).subList(0, 4));
    }
}