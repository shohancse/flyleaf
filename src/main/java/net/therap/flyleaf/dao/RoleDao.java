package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.Role;
import org.springframework.stereotype.Repository;

/**
 * @author shafin
 * @since 12/26/2016
 */
@Repository
public class RoleDao extends AbstractDao<Role> {
}
