package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.ActivityLog;
import net.therap.flyleaf.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author subrata
 * @author arafat
 * @since 12/22/16
 */
@Repository
public class ActivityLogDao extends AbstractDao<ActivityLog> {

    public List<ActivityLog> getActivityLogs(User user, int page, int size) {
        return (List<ActivityLog>) em.createQuery("FROM ActivityLog" + " WHERE user = :val ORDER by created_at desc")
                .setParameter("val", user)
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();
    }

    public List<ActivityLog> getMyActivityLogs(User user, int page, int size) {
        return (List<ActivityLog>) em.createQuery("FROM ActivityLog" + " WHERE user = :val AND activity_type " +
                "in('INSERT','UPDATE') ORDER by created_at desc")
                .setParameter("user", user)
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();
    }
}
