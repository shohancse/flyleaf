package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.OrderedProduct;
import net.therap.flyleaf.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author shafin
 * @since 12/29/16
 */
@Repository
public class OrderedProductDao extends AbstractDao<OrderedProduct> {

    @SuppressWarnings("unchecked")
    public List<OrderedProduct> findAllUserPurchasedProduct(User user) {
        return (List<OrderedProduct>) em.createQuery("SELECT DISTINCT p FROM Order o LEFT JOIN o.orderedProductList p WHERE o.user = :user")
                .setParameter("user", user)
                .getResultList();
    }
}
