package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.Store;
import net.therap.flyleaf.enumerator.Status;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author al-amin
 * @author arafat
 * @since 12/22/16
 */
@Repository
public class StoreDao extends AbstractDao<Store> {

    public List<Store> getPopularStores() {

        String sql = "SELECT * FROM store WHERE id IN(SELECT DISTINCT store_id FROM product WHERE avg_rating >= 3.5)";

        List<Store> popularStores = em.createNativeQuery(sql, Store.class).getResultList();

        return popularStores;
    }

    public Store doesExist(String searchKeyword){
        List<Store> list = em.createQuery("FROM " + Store.class.getName() + " WHERE " + "name" + " " +
                "LIKE :searchKeyword", Store.class)
                .setParameter("searchKeyword", "%"+searchKeyword+"%")
                .getResultList();

        return !list.isEmpty() ? list.get(0) : null;
    }

    public List<Store> findAllStoreOrderByStatus() {

        List<Store> storeList = findAllBy("status",Status.PENDING);
        storeList.addAll(findAllBy("status",Status.REJECTED));
        storeList.addAll(findAllBy("status",Status.APPROVED));
        return storeList;
    }
}