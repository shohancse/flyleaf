package net.therap.flyleaf.dao;

import net.therap.flyleaf.domain.User;
import org.springframework.stereotype.Repository;

/**
 * @author shafin
 * @since 12/25/16
 */
@Repository
public class UserDao extends AbstractDao<User> {
}
