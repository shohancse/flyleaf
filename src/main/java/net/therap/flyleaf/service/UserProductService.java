package net.therap.flyleaf.service;

import net.therap.flyleaf.dao.OrderedProductDao;
import net.therap.flyleaf.dao.UserDao;
import net.therap.flyleaf.dao.UserProductRatingDao;
import net.therap.flyleaf.domain.OrderedProduct;
import net.therap.flyleaf.domain.Product;
import net.therap.flyleaf.domain.User;
import net.therap.flyleaf.domain.UserProductRating;
import net.therap.flyleaf.web.command.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 12/29/16
 */
@Transactional
@Service(value = "userProductService")
public class UserProductService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserProductRatingDao userProductRatingDao;

    @Autowired
    private OrderedProductDao orderedProductDao;

    public List<ProductInfo> getUserPurchasedProducts(long userId) {
        List<ProductInfo> productInfoList = new ArrayList<>();
        User user = userDao.find(userId);
        if (user != null) {

            List<OrderedProduct> productList = orderedProductDao.findAllUserPurchasedProduct(user);
            for (OrderedProduct orderedProduct : productList) {
                productInfoList.add(UserServiceHelper.getProductInfoFromEntity(orderedProduct));
            }
        }

        return productInfoList;
    }

    public List<ProductInfo> getUserRatedProducts(long userId) {
        List<ProductInfo> productInfoList = new ArrayList<>();
        User user = userDao.find(userId);
        if (user != null) {

            List<UserProductRating> ratedProductList = userProductRatingDao.findAllBy("user", user);
            for (UserProductRating ratedProduct : ratedProductList) {

                ProductInfo productInfo = new ProductInfo();
                Product product = ratedProduct.getProduct();
                productInfo = UserServiceHelper.mapProductInfoFromEntity(productInfo, product);
                productInfo.setAvgRating(ratedProduct.getRatingValue());

                productInfoList.add(productInfo);
            }
        }

        return productInfoList;
    }
}
