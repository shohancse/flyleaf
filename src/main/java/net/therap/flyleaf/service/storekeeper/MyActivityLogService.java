package net.therap.flyleaf.service.storekeeper;

import net.therap.flyleaf.dao.ActivityLogDao;
import net.therap.flyleaf.dao.UserDao;
import net.therap.flyleaf.domain.ActivityLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Al-Amin on 12/30/2016.
 */
@Service
@Transactional
public class MyActivityLogService {
    @Autowired
    ActivityLogDao activityLogDao;

    @Autowired
    UserDao userDao;

    public List<ActivityLog> getMyActivityLog(long userId, int page, int size) {
        return activityLogDao.getMyActivityLogs(userDao.find(userId), page, size);
    }
}
