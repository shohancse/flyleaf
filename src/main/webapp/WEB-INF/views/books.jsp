<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
%>
<html>
<head>
    <title>FlyLeaf</title>

    <link href="<c:url value="/resources/css/home-style.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>

<hr>

<!-- Main Navigation Bar Start -->
<nav class="navbar-inverse">
    <div>
        <ul class="nav nav-justified">
            <li class="active"><a href="<c:url value="/categories"/>">Categories</a></li>
            <li><a href="<c:url value="/authors"/>">Authors</a></li>
            <li><a href="<c:url value="/publishers"/>">Publishers</a></li>
            <li><a href="<c:url value="/stores"/>">Book Stores</a></li>
        </ul>
    </div>
</nav>
<!-- Main Navigation Bar End -->
<hr>
<!-- Banner Image Start -->
<img class="img-responsive center-block" src="<c:url value ="/resources/images/banner.jpg" />">
<!-- Banner Image End -->


<hr>

<div class="container">
    <div class="row marketing">
        <div class="col-sm-4">
            <div class="row">
                <form:form method="GET" commandName="choiceList" action="/flyleaf/product">

                    <c:if test="${filterExcluded ne 'author'}">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Shop By Authors</div>
                            <div class="panel-body text-left">
                                <c:forEach items="${authorList}" var="author">

                                    <input type="checkbox" name="authorIds" value="${author.id}"
                                      <c:if test="${author.isChecked() eq'true'}">checked</c:if> >
                                    <c:out value="${author.name}"></c:out>

                                    <br>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    </c:if>
                    <br>

                    <c:if test="${filterExcluded ne 'category'}">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Shop By Categories</div>
                                <div class="panel-body text-left">
                                    <c:forEach items="${categoryList}" var="category">

                                        <input type="checkbox" name="categoryIds" value="${category.id}"
                                               <c:if test="${category.isChecked() eq'true'}">checked</c:if> >
                                        <c:out value="${category.name}"></c:out>
                                        <br>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <br>
                    <c:if test="${filterExcluded ne 'publisher'}">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Shop By Publishers</div>
                                <div class="panel-body text-left">
                                    <c:forEach items="${publisherList}" var="publisher">

                                        <input type="checkbox" name="publisherIds" value="${publisher.id}"
                                               <c:if test="${publisher.isChecked() eq'true'}">checked</c:if> >
                                        <c:out value="${publisher.name}"></c:out>
                                        <br>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <br>
                    <c:if test="${filterExcluded ne 'store'}">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Shop By Stores</div>
                                <div class="panel-body text-left">
                                    <c:forEach items="${storeList}" var="store">

                                        <input type="checkbox" name="storeIds" value="${store.id}"
                                               <c:if test="${store.isChecked() eq'true'}">checked</c:if> >
                                        <c:out value="${store.name}"></c:out>
                                        <br>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <input type="hidden" name="filterExcluded" value="${filterExcluded}">
                    <input type="hidden" name="choiceTypeId" value="${choiceTypeId}">
                    <input type="submit" value="Filter Result" class="btn btn-info"/>
                </form:form>
            </div>
        </div>

        <div class="col-sm-8">
            <c:forEach var="product" items="${productList}">
                <a href="<c:url value="/product/${product.id}"/>">
                    <div class="col-sm-6 col-md-4">
                        <div class="card">
                            <img src=<c:url value="/resources/images/book.jpg"></c:url>>

                            <div>
                                <h3>${product.book.title}</h3>

                                <p>Author: ${product.book.authorList[0].name}</p>

                                <p>Publisher: ${product.book.publisher.name}</p>

                                <p>Store: ${product.store.name}</p>
                            </div>
                            <div>
                                <ul class="list-inline list-unstyled">
                                    <li>Tk. ${product.basePrice}</li>
                                </ul>
                            </div>
                            <div>
                                <form method="post" action="/flyleaf/cart/add">
                                    <input type="hidden" name="productId" value="${product.id}">
                                    <input type="submit" value="Add to Cart" class="btn btn-primary"/>
                                </form>
                            </div>
                            <br>

                        </div>
                    </div>
                </a>

            </c:forEach>
        </div>

    </div>
</div>
<hr>

<!-- Pagination Starts -->
<div class="text-center">
    <ul class="pagination">
        <li><a href="#"><< previous</a></li>
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">>> next</a></li>

    </ul>
</div>

<!-- Pagination Ends -->
<!-- Footer Start -->
<hr>

<div>
    <footer style="background:black">
        <p class="pull-right"><a href="#">Back To Top</a></p>

        <p style="color:white">Designed By Company . <a href="#">Privacy</a> . <a href="#">Terms</a></p>
    </footer>
</div>
</div>
<!-- Footer End -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
