<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>FlyLeaf :: Orders</title>

    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">

        <%@include file="../jspf/userNavPill.jspf" %>

        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">
                    <h4>Billing Details&nbsp;
                        <small>
                            <button type="submit" class="btn btn-xs btn-primary pull-right"
                                    onclick="printDiv('printableArea')">
                                <i class="glyphicon glyphicon-print"></i> Print
                            </button>
                        </small>
                    </h4>
                </div>
                <div class="panel-body" id="printableArea">

                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <div class="invoice-title">
                                <h2>Invoice</h2>

                                <h3 class="pull-right">Order # <c:out value="${order.orderId}"/></h3>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-6">
                                    <address>
                                        <strong>Billed To:</strong><br>
                                        <c:out value="${order.billedTo}"/>
                                    </address>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <address>
                                        <strong>Shipped To:</strong><br>
                                        <c:out value="${order.shippingAddress}"/>
                                    </address>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <address>
                                        <strong>Payment Method:</strong><br>
                                        <c:out value="${order.paymentInfo}"/>
                                    </address>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <address>
                                        <strong>Order Date:</strong><br>
                                        <c:out value="${order.checkoutDate}"/><br><br>
                                    </address>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><strong>Order summary</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <td><strong>#&nbsp;Product</strong></td>
                                                    <td class="text-center"><strong>Description</strong></td>
                                                    <td class="text-center"><strong>Price</strong></td>
                                                    <td class="text-center"><strong>Quantity</strong></td>
                                                    <td class="text-right"><strong>Totals</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <c:forEach items="${order.productList}" var="product">
                                                    <tr>
                                                        <td><c:out value="${product.productId}"/></td>
                                                        <td class="text-center"><c:out
                                                                value="${product.bookTitle}"/></td>
                                                        <td class="text-center">TK <c:out
                                                                value="${product.price}"/></td>
                                                        <td class="text-center"><c:out
                                                                value="${product.quantity}"/></td>
                                                        <td class="text-right">TK <c:out
                                                                value="${product.subTotal}"/></td>
                                                    </tr>
                                                </c:forEach>

                                                <tr>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                                    <td class="thick-line text-right">
                                                        TK <c:out
                                                            value="${order.subTotalPrice}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-center"><strong>Shipping</strong></td>
                                                    <td class="no-line text-right">TK <c:out
                                                            value="${order.shippingCost}"/></td>
                                                </tr>
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-center"><strong>Total</strong></td>
                                                    <td class="no-line text-right">
                                                        TK <c:out value="${order.totalPrice}"/>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
</body>
</html>
