<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-6 col-sm-6">
            <div class="login-area shadow-depth-1">

                <form:form commandName="loginForm" class="form-horizontal" action="login" method="post">

                    <fieldset>
                        <!-- Form Name -->
                        <legend><spring:message code="legend.signIn"/></legend>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">
                                <spring:message code="label.email"/>
                            </label>

                            <div class="col-md-8">
                                <form:input path="email" id="email" name="email" type="text" placeholder="your email"
                                            class="form-control input-md" required="required"/>
                                <form:errors path="email" class="text-danger"/>
                            </div>
                        </div>

                        <!-- Password input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="password">
                                <spring:message code="label.password"/>
                            </label>

                            <div class="col-md-8">
                                <form:input path="password" id="password" name="password" type="password"
                                            placeholder="Your password"
                                            class="form-control input-md" required="required"/>
                                <form:errors path="password" class="text-danger"/>
                            </div>
                        </div>


                        <!-- Checkbox input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>

                            <div class="row checkbox col-md-8">
                                <label>
                                    <form:checkbox path="rememberMe" value="false"/><spring:message
                                        code="label.rememberMe"/>
                                </label>
                            </div>
                        </div>


                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="login"></label>

                            <div class="col-md-8">
                                <button id="login" name="" class="btn btn-info">
                                    <spring:message code="button.login"/>
                                </button>
                            </div>
                        </div>


                        <!-- Alert -->
                        <c:if test="${loginFail != null}">
                            <div class="alert alert-danger alert-dismissable fade in shadow-depth-2">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><spring:message code="login.failTitle"/></strong><spring:message
                                    code="login.failMessage"/>
                            </div>
                        </c:if>

                    </fieldset>
                </form:form>
            </div>
        </div>

    </div>

</div>
</body>
</html>
