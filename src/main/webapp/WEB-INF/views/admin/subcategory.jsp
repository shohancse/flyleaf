<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <div class="col-md-9 col-sm-9">

            <%--Add New SubCategory Form--%>
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.addNewSubCategory"/></div>
                <div class="panel-body">
                    <div class="col-md-4 col-sm-4">
                        <form:form action="subcategory/add" method="post">
                            <input name="name" type="text" placeholder="SubCategory Name"
                                   class="form-control input-md" required="required">
                            <br>
                            <button type="submit" class="btn btn-info">
                                <spring:message code="button.add"/>
                            </button>
                        </form:form>
                    </div>
                </div>
            </div>

            <%--Available Book SubCategory Table--%>
            <c:set var="category" value="${category}"/>
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.categoryName"/> ${category.name}</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.subCategoryId"/></th>
                            <th><spring:message code="label.subCategoryName"/></th>
                            <th><spring:message code="label.edit"/></th>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach items="${category.subCategoryList}" var="subCategory">
                            <form:form action="subcategory/update" method="post">
                                <tr>
                                    <td><input name="id" value="${subCategory.id}" readonly="true"/></td>
                                    <td><input name="name" value="${subCategory.name}" required="required"/></td>
                                    <td>
                                        <button type="submit" class="btn btn-default">
                                            <spring:message code="button.update"/>
                                        </button>
                                    </td>
                                </tr>
                            </form:form>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</body>