<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-md-3 col-sm-3">

    <div class="business-card">
        <div class="media">
            <div class="media-object" style="text-align: left">
                <h2 class="media-heading">
                    <spring:message code="label.projectTitle"/>
                </h2>
                <h4 class="media-heading">
                    <spring:message code="label.adminPanel"/>
                </h4>
            </div>
            <div class="media-right">
            </div>
        </div>
    </div>

    <div class="column">
        <ul class="nav nav-pills nav-stacked shadow-depth-1">
            <li><a href="${pageContext.servletContext.contextPath}/admin/dashboard" class="active2"><span
                    class="glyphicon glyphicon-th-large"></span> <spring:message code="pill.dashboard"/></a>
            </li>

            <li><a href="${pageContext.servletContext.contextPath}/admin/stores"><span
                    class="glyphicon glyphicon-flag"></span> <spring:message code="pill.stores"/></a>
            </li>

            <li><a href="${pageContext.servletContext.contextPath}/admin/orders"><span
                    class="glyphicon glyphicon-shopping-cart"></span> <spring:message code="pill.admin.orders"/></a>
            </li>

            <li><a href="${pageContext.servletContext.contextPath}/admin/newProducts"><span
                    class="glyphicon glyphicon-edit"></span> <spring:message code="pill.productVerification"/></a>
            </li>

            <li><a href="${pageContext.servletContext.contextPath}/admin/categories"><span
                    class="glyphicon glyphicon-eye-close"></span> <spring:message code="pill.category"/></a>
            </li>

            <li><a href="${pageContext.servletContext.contextPath}/admin/activities/0"><span
                    class="glyphicon glyphicon-th-list"></span> <spring:message code="pill.activityLog"/></a>
            </li>

            <li><a href="${pageContext.servletContext.contextPath}/admin/logout"><span
                    class="glyphicon glyphicon-log-out"></span> <spring:message code="pill.logout"/></a>
            </li>
        </ul>
    </div>

</div>